import java.util.Scanner;

public class Main {
    public static void main(String[] args)
        {
            String Str1,Str2;
            Scanner scan=new Scanner(System.in);
            Str1=scan.next();
            Str2=scan.next();
            Str1 = Str1.toLowerCase();
            Str2 = Str2.toLowerCase();
            System.out.println(Compare(Str1,Str2));
        }

    static int Compare(String Str1, String Str2)//Comparing two Strings
        {
            char[] String1=Str1.toCharArray();
            char[] String2=Str2.toCharArray();
            for (int i =0;i<String1.length&&i<String2.length;i++)
            {
                if (String1[i] == String2[i])
                {
                    continue;
                }
                if (String1[i] < String2[i])return -1;
                else if (String1[i] > String2[i])return 1;
            }
            return 0;
        }
}
